<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/register" method="POST">
        @csrf
      <label for="fname">First name:</label><br />
      <input type="text" id="fname" name="fname" /><br />
      <label for="lname">Last name:</label><br />
      <input type="text" id="lname" name="lname" />

      <p>Gender:</p>
      <input type="radio" id="html" name="fav_language" value="Male" />
      <label for="Male">Male</label><br />
      <input type="radio" id="Female" name="fav_language" value="Female" />
      <label for="Female">Female</label><br />
      <input type="radio" id="Other" name="fav_language" value="Other" />
      <label for="Other">Other</label><br />

      <br />
      <label for="Nationality">Nationality:</label><br />
      <select name="Nationality" id="Nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="Singaporean">Singaporean</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Australian">Australian</option>
      </select><br />

      <br />
      <label for="LanguageSpoken">Language Spoken:</label><br />
      <input type="checkbox" id="BahasaIndonesia" name="BahasaIndonesia" value="Bike" />
      <label for="BahasaIndonesia">Bahasa Indonesia</label><br />
      <input type="checkbox" id="English" name="English" value="Car" />
      <label for="English"> English</label><br />
      <input type="checkbox" id="Other" name="Other" value="Boat" />
      <label for="Other"> Other</label><br />

      <br />
      <label for="Bio">Bio:</label><br />
      <textarea name="Bio" form="Bio" rows="10" cols="30"></textarea>
    <br />
      <button type="submit" value="Sign Up">Sign Up</button>
    </form>
    
  </body>
</html>
